module DetectPII

using CSV
using DataFrames

export detectpii

include("pii_strings.jl")

"""
    detectpii(dir = "."; kwargs...) -> DataFrame

Scan directory for data files and detect potential PII variables.

# Arguments
- `dir::String = "."`: the relative or absolute directory path to scan

# Keywords
- `out::String = ""`: the CSV filename to save results to (using relative or absolute path)
- `add::Vector{String} = String[]`: keywords to add to search
- `remove::Vector{String} = String[]`: keywords to remove from search
- `ignore::Vector{String} = String[]`: variable names to ignore in search
- `exact::Bool = false`: match keywords exactly

# Returns
- `DataFrame`: a DataFrame of results (filepath, pii_variable)

# Throws
- `ArgumentError`: if directory to scan does not exist

# Examples
```
detectpii()
detectpii("data")
detectpii("data"; out = "pii_results.csv")
detectpii("data"; add = ["respondent"])
detectpii("data"; remove = ["birth"])
detectpii("data"; ignore = ["gender"])
detectpii("data"; exact = true)
```
"""
function detectpii(
    dir::String = ".";
    out::String = "",
    add::Vector{String} = String[],
    remove::Vector{String} = String[],
    ignore::Vector{String} = String[],
    exact::Bool = false
)
    if !ispath(dir)
        return throw(ArgumentError("Directory does not exist!"))
    end

    println("Scanning directory: ", dir)

    # Get vector of filepaths with recursion into subdirectories
    files = [(joinpath.(root, files) for (root, dir, files) in walkdir(dir))...;]

    # Initialize empty DataFrame for storing results
    df = DataFrame()

    # Scan files
    if isempty(files)
        println("No files in directory!")
    else
        for file in files
            results = scanfile(file, pii_strings; add, remove, ignore, exact)
            append!(df, results)
        end
    end

    println("Scanning complete!")

    # Save results to CSV file if requested
    if out != ""
        CSV.write(out, df)
        println("Results saved to ", out)
    end

    return df
end

"""
    scanfile(path, keywords; kwargs...) -> DataFrame

Scan variable names in file for keywords.

# Arguments
- `path::String`: a filepath
- `keywords::Vector{String}`: the keywords to search for

# Keywords
- `add::Vector{String} = String[]`: keywords to add to search
- `remove::Vector{String} = String[]`: keywords to remove from search
- `ignore::Vector{String} = String[]`: variable names to ignore in search
- `exact::Bool = false`: match keywords exactly

# Returns
- `DataFrame`: a DataFrame of results (filepath, pii_variable)
"""
function scanfile(
    path::String,
    keywords::Vector{String};
    add::Vector{String} = String[],
    remove::Vector{String} = String[],
    ignore::Vector{String} = String[],
    exact::Bool = false
)
    if split(path, ".")[end] == "csv"
        data = CSV.read(path, DataFrame, limit = 1, types = String, ntasks = 1)
    else
        println("File format not supported: ", path)
        results = DataFrame()
        return results
    end

    # Construct vector of keywords
    # Add keywords based on add kwarg
    if add != String[]
        keywords = vcat(keywords, add)
    end
    # Remove keywords based on remove kwarg
    if remove != String[]
        keywords = filter(x -> !(x in remove), keywords)
    end

    # Construct vector of varnames
    # Ignore varnames based on ignore kwarg
    if ignore != String[]
        varnames = filter(x -> !(x in ignore), names(data))
    else
        varnames = names(data)
    end

    # Initialize empty string vector for storing variable names with matches
    matches = String[]

    for name in varnames
        res = scanvar(name, keywords; exact)
        if res != ""
            push!(matches, res)
        end
    end

    if isempty(matches)
        results = DataFrame()
    else
        results = DataFrame([fill(path, length(matches)), matches], ["filepath", "pii_variable"])
    end

    return results
end

"""
    scanvar(var, keywords; exact = false) -> String

Scan variable name for keywords.

# Arguments
- `var::String`: the variable name to scan
- `keywords::Vector{String}`: the keywords to search for

# Keywords
- `exact::Bool = false`: match keywords exactly

# Returns
- `String`: if match, variable name; if no matches, empty string
"""
function scanvar(
    var::String,
    keywords::Vector{String};
    exact::Bool = false
)
    # Lowercase variable name so capitalization can be ignored
    var = lowercase(var)

    if exact
        # Search for keywords as exact matches to variable name
        rgx = "^" .* keywords .* "\$"
        rgx = Regex(join(rgx, "|"))
        if isa(match(rgx, var), RegexMatch)
            return var
        else
            return ""
        end
    else
        # Search for keywords in variable name
        if any(occursin.(keywords, var))
            return var
        else
            return ""
        end
    end
end

end # module DetectPII
