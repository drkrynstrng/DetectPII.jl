# Strings to search for that may indicate PII

pii_strings_names = [
    "name",
    "fname",
    "lname",
    "firstname",
    "lastname",
    "first_name",
    "last_name"
]

pii_strings_dates = [
    "birth",
    "birthday",
    "birthdate",
    "bday",
    "dob"
]

pii_strings_locations = [
    "loc",
    "location",
    "city",
    "municipality",
    "district",
    "lc",
    "parish",
    "village",
    "clinic",
    "community",
    "territory",
    "panchayat",
    "precinct",
    "block",
    "cell",
    "zip",
    "zipcode",
    "county",
    "subcounty",
    "country",
    "subcountry",
    "nation",
    "nationality",
    "address",
    "street",
    "house",
    "household",
    "compound",
    "gps",
    "degree",
    "minute",
    "second",
    "lat",
    "latitude",
    "lon",
    "longitude",
    "coord",
    "coordinate",
    "coordinates"
]

pii_strings_networks = [
    "network",
    "school",
    "work",
    "employer",
    "census",
    "child",
    "beneficiary",
    "mother",
    "father",
    "wife",
    "husband",
    "spouse",
    "daughter",
    "son",
    "brother",
    "sister"
]

pii_strings_other = [
    "age",
    "race",
    "ethnicity",
    "gender",
    "sex",
    "contact",
    "phone",
    "mobile",
    "landline",
    "fax",
    "email",
    "ip",
    "url",
    "job",
    "position",
    "salary",
    "grade",
    "credit",
    "debit",
    "bank",
    "routing",
    "account",
    "driver",
    "license",
    "vin",
    "social",
    "ssn",
    "tin",
    "username"
]

# Create single vector of strings
pii_strings = reduce(vcat, (pii_strings_names,
                            pii_strings_dates,
                            pii_strings_locations,
                            pii_strings_networks,
                            pii_strings_other))
