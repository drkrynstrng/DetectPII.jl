# DetectPII.jl

*Scan data files to detect PII.*

The DetectPII package provides the function `detectpii()` that recursively scans a specified directory for data files and detects potential personally identifiable information (PII) within the data files based on variable names. The detection is based on matching variable names to certain keyword strings. See the strings used [here](src/pii_strings.jl).

Currently supported file formats include:

- CSV (read via CSV.jl)

## Installation

The package is not currently registered, but it can be installed from GitLab:

```
] add https://gitlab.com/drkrynstrng/DetectPII.jl
```

## Usage

By default, `detectpii()` recursively scans the current working directory for data files. You can also specify a relative or absolute directory path. For CSV files, the function assumes that variable names are stored in the first row of the file. Results are returned as a DataFrame with two variables: filepath and pii_variable. These results can optionally be saved to a CSV file. Note that some variables in the results may not necessarily contain PII.

Some examples:

```
detectpii()
detectpii("data")
detectpii("data"; out = "pii_results.csv")
detectpii("data"; add = ["respondent"])
detectpii("data"; remove = ["birth"])
detectpii("data"; ignore = ["gender"])
detectpii("data"; exact = true)
```

## License

[0BSD](LICENSE)
