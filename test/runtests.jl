using DetectPII
using Test
using DataFrames

@testset "DetectPII" begin
    @test_throws MethodError DetectPII.detectpii(1)
    @test_throws ArgumentError DetectPII.detectpii("asdf")

    @test_throws TypeError DetectPII.detectpii("asdf"; out = 1)
    tmpcsv = joinpath(tempdir(), "pii_results.csv")
    DetectPII.detectpii("data"; out = tmpcsv)
    @test isfile(tmpcsv)
    rm(tmpcsv)

    @test_throws TypeError DetectPII.detectpii("asdf"; add = 1)
    @test_throws TypeError DetectPII.detectpii("asdf"; add = "asdf")
    results = DetectPII.detectpii("data"; add = ["allergy"])
    @test any(occursin.("allergy", results[!, "pii_variable"]))
    results = DetectPII.detectpii("data"; add = ["allergy", "favorite"])
    @test any(occursin.("allergy", results[!, "pii_variable"]))
    @test any(occursin.("favorite", results[!, "pii_variable"]))

    @test_throws TypeError DetectPII.detectpii("asdf"; remove = 1)
    @test_throws TypeError DetectPII.detectpii("asdf"; remove = "asdf")
    results = DetectPII.detectpii("data"; remove = ["address"])
    @test !any(occursin.("address", results[!, "pii_variable"]))
    results = DetectPII.detectpii("data"; remove = ["address", "gender"])
    @test !any(occursin.("address", results[!, "pii_variable"]))
    @test !any(occursin.("gender", results[!, "pii_variable"]))

    @test_throws TypeError DetectPII.detectpii("asdf"; ignore = 1)
    @test_throws TypeError DetectPII.detectpii("asdf"; ignore = "asdf")
    results = DetectPII.detectpii("data"; ignore = ["gender"])
    @test !any(occursin.("gender", results[!, "pii_variable"]))
    results = DetectPII.detectpii("data"; ignore = ["gender", "birthdate"])
    @test !any(occursin.("gender", results[!, "pii_variable"]))
    @test !any(occursin.("birthdate", results[!, "pii_variable"]))

    @test_throws TypeError DetectPII.detectpii("asdf"; exact = 1)
    @test_throws TypeError DetectPII.detectpii("asdf"; exact = "asdf")
    results = DetectPII.detectpii("data"; exact = true)
    @test !any(occursin.("fullname", results[!, "pii_variable"]))

    results = DetectPII.detectpii("data")
    @test isa(results, DataFrame)
    @test any(occursin.(".csv", results[!, 1]))
    @test !any(occursin.(".dta", results[!, 1]))
    @test !any(occursin.(".sas7bdat", results[!, 1]))
    @test !any(occursin.(".por", results[!, 1]))
    @test !any(occursin.(".sav", results[!, 1]))
end
